# Workshop - Rust Intro

> Video of the session: https://youtu.be/5NrbKBx-Ipk

- Rust is (mainly) a systems programming language designed for safety, speed, and concurrency. 
- Rust has numerous compile-time features and safety checks to avoid common bugs
- An interesting usage of Rust is in the WebAssembly community

# The Basic Stuff

Where to find help/ref: https://doc.rust-lang.org/

## Installation with rustup: 

```sh
$ curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh
```

More options: https://doc.rust-lang.org/book/ch01-01-installation.html

## Basic code

```rust
fn main() {
    println!("Hello, world!");
}
```

### Compiling and running

```sh
sachinmuralig@mjolnir src % ls
main.rs
sachinmuralig@mjolnir src % rustc main.rs
sachinmuralig@mjolnir src % ls   
main	main.rs
sachinmuralig@mjolnir src % ./main
```

## Cargo

- Cargo is Rust’s build system and package manager. 
- Handles a lot of tasks such as building, downloading and building the dependencies etc

### New project with Cargo

```sh
sachinmuralig@mjolnir rust_learning % cargo new hello-rust  
     Created binary (application) `hello-rust` package
sachinmuralig@mjolnir rust_learning % cd hello-rust 
sachinmuralig@mjolnir hello-rust % tree
.
├── Cargo.toml
└── src
    └── main.rs

1 directory, 2 files
sachinmuralig@mjolnir hello-rust % cargo run
   Compiling hello-rust v0.1.0 (/Users/sachinmuralig/rust_learning/hello-rust)
    Finished dev [unoptimized + debuginfo] target(s) in 1.38s
     Running `target/debug/hello-rust`   # The debug binary, unoptimized
Hello, world!
```

- `cargo build --release` will produce the optimised binary for say prod. 


### Packages and Crates

- A crate is a binary or library.
- The crate root is a source file that the Rust compiler starts from and makes up the root module of your crate
- Crate groups related functionality together in a scope

- Package is one or more crates that provides a set of functionality. 
- Package contains a `Cargo.toml` file that describes how to build those crates. `Cargo.toml` is the manifest file. (Think of it as `package.json` in npm world)

- Cargo follows a convention that `src/main.rs` is the crate root of a binary crate with the same name as the package. 
- If the package directory contains `src/lib.rs`, cargo assumes the package contains a library crate with the same name as the package, and` src/lib.rs` is its crate root.
- If a package contains `src/main.rs` and `src/lib.rs`, it has two crates: a library and a binary, both with the same name as the package
- A package can have multiple binary crates by placing files in the `src/bin` directory: each file will be a separate binary crate.

- A module is a collection of items: functions, structs etc

# Testing

- Unit tests are usually put in a module called test


```rust
// src/lib.rs

pub fn adder(a:i32, b:i32) -> i32 {
    a + b
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn internal() {
        assert_eq!(4, adder(2, 2));
    }
}
```

- Integ tests are to be put in `tests` folder at project root

```
use mycrate::myfunc;

#[test]
fn test_myfunc() {
    assert_eq!(test_myfunc(), "success");
}
```

# Exercise

- A simple calculator that takes in 2 ints (values between -1000 and +1000), and an operator(+,-,/ or * ) performs the respective operation and prints the result.

Help:

- Reading from command line:
```
use std::io;
let mut oper_a = String::new();
io::stdin()
        .read_line(&mut oper_a)
        .expect("Failed to read line");

let oper_a: i32 = oper_a.trim().parse().unwrap();
```

