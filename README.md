# learning-rust

This repo is a collaboration space for the docs and exercises during the Saturday Study related to Rust.

## Conventions

- The `docs` folder contains the materials prefixed with the chapter number (eg: `c01-`)
- The exercises folder contains the code tryouts in respective folder named after the chapter (eg: `exercises/c01/sachin/calculator`)

